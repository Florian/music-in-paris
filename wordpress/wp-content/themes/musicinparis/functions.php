<?php

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

// Disable support for comments and trackbacks in post types
function df_disable_comments_post_types_support() {
    $post_types = get_post_types();
    foreach ($post_types as $post_type) {
        if(post_type_supports($post_type, 'comments')) {
            remove_post_type_support($post_type, 'comments');
            remove_post_type_support($post_type, 'trackbacks');
        }
    }
}
add_action('admin_init', 'df_disable_comments_post_types_support');

// Close comments on the front-end
function df_disable_comments_status() {
    return false;
}
add_filter('comments_open', 'df_disable_comments_status', 20, 2);
add_filter('pings_open', 'df_disable_comments_status', 20, 2);

// Hide existing comments
function df_disable_comments_hide_existing_comments($comments) {
    $comments = array();
    return $comments;
}
add_filter('comments_array', 'df_disable_comments_hide_existing_comments', 10, 2);

// Remove comments page in menu
function df_disable_comments_admin_menu() {
    remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'df_disable_comments_admin_menu');

// Redirect any user trying to access comments page
function df_disable_comments_admin_menu_redirect() {
    global $pagenow;
    if ($pagenow === 'edit-comments.php') {
        wp_redirect(admin_url()); exit;
    }
}
add_action('admin_init', 'df_disable_comments_admin_menu_redirect');


// Remove comments links from admin bar
function df_disable_comments_admin_bar() {
    if (is_admin_bar_showing()) {
        remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
    }
}
add_action('init', 'df_disable_comments_admin_bar');


add_action( 'save_post', function( $post_id ) {
  if ( class_exists( 'WP_REST_Cache' ) ) {
    WP_REST_Cache::empty_cache();
  }
} );

add_action( 'init', 'create_post_type' );
function create_post_type() {
  register_post_type( 'venues',
    array(
      'labels' => array(
        'name' => __( 'Lieux' ),
        'singular_name' => __( 'Lieu' )
      ),
      'taxonomies'  => array( 'category', 'mood', 'space', 'genre', 'activity' ),
      'show_in_rest' => true,
      'public' => true
    )
  );
  register_post_type( 'events',
    array(
      'labels' => array(
        'name' => __( 'Événements' ),
        'singular_name' => __( 'Événement' )
      ),
      'taxonomies'  => array( 'post_tag' ),
      'show_in_rest' => true,
      'public' => true
    )
  );
  register_post_type( 'curators',
    array(
      'labels' => array(
        'name' => __( 'Curateurs' ),
        'singular_name' => __( 'Curateur' )
      ),
      'taxonomies'  => array( 'types' ),
      'show_in_rest' => true,
      'public' => true
    )
  );
}

// Let us create Taxonomy for Custom Post Type
add_action( 'init', 'create_taxonomies', 0 );
 
//create a custom taxonomy name it "type" for your posts
function create_taxonomies() {
  register_taxonomy('types',array('curators'), array(
    'hierarchical' => true,
    'labels' => array(
      'name' => __( 'Types' ),
      'singular_name' => __( 'Type' )
    ),
    'show_in_rest' => true,
    'public' => true
  ));
  register_taxonomy('mood',array('venues'), array(
    'hierarchical' => false,
    'labels' => array(
      'name' => __( 'Ambiances' ),
      'singular_name' => __( 'Ambiance' )
    ),
    'show_in_rest' => true,
    'public' => true
  ));
  register_taxonomy('space',array('venues'), array(
    'hierarchical' => false,
    'labels' => array(
      'name' => __( 'Espaces' ),
      'singular_name' => __( 'Espace' )
    ),
    'show_in_rest' => true,
    'public' => true
  ));
  register_taxonomy('genre',array('venues'), array(
    'hierarchical' => false,
    'labels' => array(
      'name' => __( 'Styles' ),
      'singular_name' => __( 'Style' )
    ),
    'show_in_rest' => true,
    'public' => true
  ));
  register_taxonomy('activity',array('venues'), array(
    'hierarchical' => false,
    'labels' => array(
      'name' => __( 'Activités' ),
      'singular_name' => __( 'Activité' )
    ),
    'show_in_rest' => true,
    'public' => true
  ));
}

function my_acf_init() {
    acf_update_setting('google_api_key', 'AIzaSyASP2DoYa1SFxYQnT_BQL_TcfRign1etbU');
}
add_action('acf/init', 'my_acf_init');

add_filter( 'rest_user_query', function( $args ) {
  $fields = array( 'field1', 'field2', 'field3', 'field4' );

  foreach ( $fields as $field ) {
          if ( isset( $_GET[ $field ] ) && ! empty( $_GET[ $field ] ) ) {
                  $args['meta_query'][] = array(
                          'key'   => $field,
                          'value' => esc_sql( $_GET[ $field ] ),
                  );
          }
  }
  return $args; 
} );


add_image_size( '1920', 1920, 9999 );
add_image_size( '1280', 1280, 9999 );
add_image_size( '960', 960, 9999 );
add_image_size( '768', 768, 9999 );
add_image_size( '375', 375, 9999 );
add_image_size( '170', 170, 9999 );
add_image_size( 'placeholder', 20, 9999 );

?>
