import React, { useState, useEffect, useContext } from 'react'
import styled from 'styled-components'
import * as moment from 'moment'

import I18nContext from 'utils/i18nContext'
import Title from 'components/misc/Title'
import Event from './events/Event'

const Wrapper = styled.div``
export default function Events(props) {
  const { translate } = useContext(I18nContext)

  const [events, setEvents] = useState([])
  useEffect(() => {
    if (props.venue) {
      setEvents(
        props.events
          .filter(
            event => event.acf.lieu && event.acf.lieu[0] === props.venue.id
          )
          .sort((a, b) =>
            moment(b.acf.horaires.debut).diff(moment(a.acf.horaires.debut)) < 0
              ? 1
              : -1
          )
          .map(event => {
            for (let curator of props.curators) {
              for (let eventId of curator.acf['evenements']) {
                if (eventId === event.id) {
                  event.curator = curator
                }
              }
            }
            return event
          })
      )
    } else {
      if (props.events && props.curator) {
        let curatorEvents = []

        for (let curatorEvent of props.curator.acf.evenements) {
          if (props.events.find(event => event.id === curatorEvent)) {
            curatorEvents.push(
              props.events.find(event => event.id === curatorEvent)
            )
          }
        }
        setEvents(curatorEvents)
      }
    }
  }, [props.events, props.curators, props.curator, props.venue])

  return events.length ? (
    <Wrapper>
      {!props.noTitle && (
        <Title>
          <span>{translate('Evenements')}</span>
        </Title>
      )}
      {events.map(event => (
        <Event key={event.id} event={event} tags={props.tags} />
      ))}
    </Wrapper>
  ) : (
    ''
  )
}
