import React from 'react'
import styled from 'styled-components'

import { colors } from 'utils/styles'

const Wrapper = styled.div`
  position: fixed;
  z-index: 9999;
  display: flex;
  justify-content: center;
  align-items: center;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  opacity: ${props => (props.open ? '1' : '0')};
  pointer-events: ${props => (props.open ? 'inherit' : 'none')};
  transition: opacity 300ms ease-out;
`
const Background = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.3);
  cursor: pointer;
`
const Content = styled.div`
  position: relative;
  z-index: 12;
  padding: 2em;
  background-color: ${colors.background};
  border: 1px solid ${colors.main};
  overflow-y: scroll;
`
export default function Modal(props) {
  return (
    <Wrapper open={props.open}>
      <Background onClick={() => props.setOpen(false)} />
      <Content size={props.size}>{props.children}</Content>
    </Wrapper>
  )
}
