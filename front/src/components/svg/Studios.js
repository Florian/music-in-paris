import React from 'react'

import { colors } from 'utils/styles'

export default function Studios(props) {
  return (
    <svg
      x='0px'
      y='0px'
      viewBox='0 0 198.19 197.26'
      style={{ width: props.width || '100%', height: props.height || '100%' }}
    >
      <path
        style={{ fill: props.colors || colors.main }}
        d='M50.27,116.02c-9.65,0-17.5-7.85-17.5-17.5s7.85-17.5,17.5-17.5s17.5,7.85,17.5,17.5
    S59.92,116.02,50.27,116.02z M50.27,86.02c-6.89,0-12.5,5.61-12.5,12.5s5.61,12.5,12.5,12.5s12.5-5.61,12.5-12.5
    S57.16,86.02,50.27,86.02z'
      />
      <path
        style={{ fill: props.colors || colors.main }}
        d='M92.27,91.02c-9.65,0-17.5-7.85-17.5-17.5s7.85-17.5,17.5-17.5s17.5,7.85,17.5,17.5
    S101.92,91.02,92.27,91.02z M92.27,61.02c-6.89,0-12.5,5.61-12.5,12.5s5.61,12.5,12.5,12.5s12.5-5.61,12.5-12.5
    S99.16,61.02,92.27,61.02z'
      />
      <path
        style={{ fill: props.colors || colors.main }}
        d='M134.27,133.02c-9.65,0-17.5-7.85-17.5-17.5s7.85-17.5,17.5-17.5s17.5,7.85,17.5,17.5
    S143.92,133.02,134.27,133.02z M134.27,103.02c-6.89,0-12.5,5.61-12.5,12.5s5.61,12.5,12.5,12.5s12.5-5.61,12.5-12.5
    S141.16,103.02,134.27,103.02z'
      />
      <path
        style={{ fill: props.colors || colors.main }}
        d='M92.27,156.02c-1.38,0-2.5-1.12-2.5-2.5v-65c0-1.38,1.12-2.5,2.5-2.5s2.5,1.12,2.5,2.5v65
    C94.77,154.9,93.65,156.02,92.27,156.02z'
      />
      <path
        style={{ fill: props.colors || colors.main }}
        d='M134.27,103.02c-1.38,0-2.5-1.12-2.5-2.5v-65c0-1.38,1.12-2.5,2.5-2.5s2.5,1.12,2.5,2.5v65
    C136.77,101.9,135.65,103.02,134.27,103.02z'
      />
      <path
        style={{ fill: props.colors || colors.main }}
        d='M50.27,86.02c-1.38,0-2.5-1.12-2.5-2.5v-48c0-1.38,1.12-2.5,2.5-2.5s2.5,1.12,2.5,2.5v48
    C52.77,84.9,51.65,86.02,50.27,86.02z'
      />
      <path
        style={{ fill: props.colors || colors.main }}
        d='M50.27,157.02c-1.38,0-2.5-1.12-2.5-2.5v-31c0-1.38,1.12-2.5,2.5-2.5s2.5,1.12,2.5,2.5v31
    C52.77,155.9,51.65,157.02,50.27,157.02z'
      />
      <path
        style={{ fill: props.colors || colors.main }}
        d='M134.27,157.02c-1.38,0-2.5-1.12-2.5-2.5v-14c0-1.38,1.12-2.5,2.5-2.5s2.5,1.12,2.5,2.5v14
    C136.77,155.9,135.65,157.02,134.27,157.02z'
      />{' '}
      <path
        style={{ fill: props.colors || colors.main }}
        d='M93.27,52.02c-1.38,0-2.5-1.12-2.5-2.5v-14c0-1.38,1.12-2.5,2.5-2.5s2.5,1.12,2.5,2.5v14
    C95.77,50.9,94.65,52.02,93.27,52.02z'
      />
    </svg>
  )
}
