import React from 'react'
import styled from 'styled-components'
import { useParams } from 'react-router-dom'

import { colors } from 'utils/styles'

const Wrapper = styled.div`
  position: absolute;
  z-index: 5;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow-y: scroll;
  padding: 3em 1em 1em;
  background-color: ${colors.background};
  transform: translateX(${props => (props.show ? '0' : '100%')});
  transition: transform 300ms ease-out;
`
const Title = styled.h1`
  color: ${colors.curators};
`

export default function VenuesList(props) {
  const { id } = useParams()

  return (
    <Wrapper show={id === 'list'}>
      {props.title && <Title>{props.title}</Title>}
      {props.children}
    </Wrapper>
  )
}
