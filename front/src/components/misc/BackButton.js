import React from 'react'
import styled from 'styled-components'

import { colors } from 'utils/styles'

const Wrapper = styled.div`
  position: fixed;
  z-index: 6;
  top: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 50px;
  height: 50px;
  cursor: pointer;

  &:before {
    content: '';
    position: absolute;
    top: 50%;
    right: 0;
    transform: translateY(-50%);
    width: 1px;
    height: 70%;
    background-color: #08175f;
  }
`
const Svg = styled.svg`
  height: 26px;
`
export default function BackButton(props) {
  return (
    <Wrapper onClick={props.onClick}>
      <Svg height='512' viewBox='0 0 546.55 546.55' width='512'>
        <path
          style={{ fill: props.colors || colors.main }}
          d='m290.355 478.233c-3.386 0-6.772-1.017-9.674-3.002l-273.275-187.877c-4.637-3.186-7.406-8.457-7.406-14.077 0-5.621 2.769-10.892 7.406-14.077l273.275-187.877c5.221-3.586 12.026-4.003 17.63-1.034 5.604 2.952 9.124 8.773 9.124 15.112v85.398h222.035c9.44 0 17.08 7.639 17.08 17.08v170.797c0 9.441-7.639 17.08-17.08 17.08h-222.036v85.398c0 6.338-3.519 12.159-9.124 15.112-2.501 1.316-5.237 1.967-7.955 1.967zm-243.136-204.957 226.056 155.402v-70.004c0-9.441 7.639-17.08 17.08-17.08h222.036v-136.636h-222.036c-9.441 0-17.08-7.639-17.08-17.08v-70.003z'
        />
      </Svg>
    </Wrapper>
  )
}
