import styled from 'styled-components'

import { colors } from 'utils/styles'

export default styled.h3`
  position: relative;
  margin-bottom: 1em;
  font-size: 16px;
  font-weight: normal;
  text-transform: uppercase;

  &:before {
    content: '';
    position: absolute;
    top: 50%;
    left: 0;
    transform: translateY(-50%);
    width: 100%;
    height: 1px;
    background-color: ${colors.main};
  }
  span {
    position: relative;
    margin: 0 1.7em;
    padding: 0 0.5em;
    color: ${colors.main};
    background-color: ${colors.background};
  }
`
