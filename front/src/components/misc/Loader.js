import React, { useState, useEffect, useContext } from 'react'
import styled, { keyframes } from 'styled-components'

import { colors } from 'utils/styles'
import useWindowHeight from 'hooks/useWindowHeight'
import ApiContext from 'utils/apiContext'

const Wrapper = styled.div`
  position: fixed;
  z-index: 900;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: ${props => props.windowHeight}px;
  background-color: ${colors.background};
  opacity: ${props => (props.venuesLoaded ? 0 : 1)};
  pointer-events: ${props => (props.venuesLoaded ? 'none' : 'inherit')};
  transition: all 600ms;
`
const flash = keyframes`
  0% {
    opacity: 0;
  }
  30% {
    opacity: 0.8;
  }
  50% {
    opacity: 1;
  }
  70% {
    opacity: 0.8;
  }
  100% {
    opacity: 0;
  }
`
const Text = styled.h1`
  margin: 0;
  font-size: 10vw;
  animation: ${flash} 3s linear infinite;
`
const Small = styled.span`
  font-size: 0.6em;
  font-weight: normal;
  color: ${colors.main};
`
export default function Loader() {
  const { lives, clubs, studios, records, curators, events } = useContext(
    ApiContext
  )
  const [venuesLoaded, setVenuesLoaded] = useState(false)
  useEffect(() => {
    if (
      lives.length &&
      clubs.length &&
      studios.length &&
      records.length &&
      curators.length
    ) {
      setVenuesLoaded(true)
    }
  }, [lives, clubs, studios, records, curators, events])

  const windowHeight = useWindowHeight()

  return (
    <Wrapper windowHeight={windowHeight} venuesLoaded={venuesLoaded}>
      <Text>
        Music <Small>in</Small> Paris
      </Text>
    </Wrapper>
  )
}
