import React, { useState } from 'react'
import styled from 'styled-components'
import Hammer from 'react-hammerjs'

import { colors, mq } from 'utils/styles'
import useWindowHeight from 'hooks/useWindowHeight'

const Wrapper = styled.div`
  position: absolute;
  z-index: 10;
  top: ${props => props.position}px;
  left: 0;
  display: flex;
  height: ${props => (props.open ? props.windowHeight + 'px' : '40vw')};
  width: 100vw;
  background-color: ${colors.background};
  overflow: scroll;
  transform: translateY(
    ${props =>
      props.show
        ? props.open
          ? '0'
          : 'calc(' + props.windowHeight + 'px - 40vw)'
        : props.windowHeight + 'px'}
  );
  transition: transform 300ms ease-out,
    height 300ms ease-out ${props => !props.dragging && ', top 300ms ease-out'};
  box-shadow: 0px -10px 25px 0px rgba(0, 0, 0, 0.6);

  ${mq.medium} {
    height: ${props => (props.open ? props.windowHeight + 'px' : '250px')};
    transform: translateY(
      ${props =>
        props.show
          ? props.open
            ? '0'
            : 'calc(' + props.windowHeight + 'px - 250px)'
          : props.windowHeight + 'px'}
    );
  }
`
const Content = styled.div`
  width: 100%;
  height: 100%;
`
export default function VerticalDrawer(props) {
  const [position, setPosition] = useState(0)
  const [dragging, setDragging] = useState(false)

  const windowHeight = useWindowHeight()

  return (
    <Wrapper
      show={props.show}
      open={props.open}
      position={position}
      dragging={dragging}
      windowHeight={windowHeight}
    >
      {!props.open ? (
        <Hammer
          onClick={() => props.openDrawer()}
          onPan={e => {
            setDragging(!e.isFinal)

            if (e.isFinal) {
              if (position < -40) {
                props.openDrawer()
              }
              setPosition(0)
            } else {
              setPosition(e.deltaY)
            }
          }}
          direction={'DIRECTION_ALL'}
        >
          <Content>{props.children}</Content>
        </Hammer>
      ) : (
        props.children
      )}
    </Wrapper>
  )
}
