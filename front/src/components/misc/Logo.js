import React, { useContext } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import { colors, mq } from 'utils/styles'
import SearchContext from 'utils/searchContext'

const Wrapper = styled.div`
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  right: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 50px;
  background-color: ${colors.background};
  box-shadow: 0px 10px 25px 0px rgba(0, 0, 0, 0.6);

  a {
    text-decoration: none;
  }

  ${mq.large} {
    display: none;
  }
`
const Text = styled.h1`
  margin: 0;
`
const Small = styled.span`
  font-size: 0.6em;
  font-weight: normal;
  color: ${colors.main};
`
export default function Logo() {
  const search = useContext(SearchContext)

  return (
    <Wrapper
      onClick={() => {
        search.setCategory()
        search.setMoods([])
        search.setSpaces([])
        search.setGenres([])
        search.setActivities([])
      }}
    >
      <Link to='/'>
        <Text>
          Music <Small>in</Small> Paris
        </Text>
      </Link>
    </Wrapper>
  )
}
