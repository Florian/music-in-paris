import React from 'react'
import styled from 'styled-components'

import { colors } from 'utils/styles'
import { getSrcset } from 'utils/img'
import Curated from 'components/svg/Curated'

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 0.2em;
  color: ${colors.main};
`
const Dot = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 0.7em;
  height: 0.7em;
  margin-right: 0.5em;
  background-color: ${colors.main};
  border-radius: 50%;
`
const Logo = styled.img`
  height: 1em;
  width: auto;
  margin-left: 0.4em;
`
export default function Curator(props) {
  return (
    <Wrapper>
      <Dot>
        <Curated colors={colors.background} width='80%' height='80%' />
      </Dot>
      <span>Recommandé par</span>
      <Logo
        key={props.curator.acf.logo.id}
        src={props.curator.acf.logo.url}
        alt={props.curator.acf.logo.alt}
        srcSet={getSrcset(props.curator.acf.logo.sizes)}
        size={'100px'}
      />
    </Wrapper>
  )
}
