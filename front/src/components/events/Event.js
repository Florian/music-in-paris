import React, { useContext } from 'react'
import styled from 'styled-components'
import * as moment from 'moment'
import 'moment/locale/fr'

import { colors } from 'utils/styles'
import I18nContext from 'utils/i18nContext'
import Tags from 'components/Tags'
import Curator from './event/Curator'

const Wrapper = styled.a`
  display: flex;
  justify-content: space-between;
  margin-bottom: 1em;
  text-decoration: none;
`
const Content = styled.div`
  flex: 1;
  margin-right: 1em;
`
const Title = styled.h3`
  margin: 0 0 0.1em;
`
const Description = styled.div`
  p {
    font-size: 12px;
    margin: 0 0 0.3em;
  }
`
const Time = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 50px;
  height: 50px;
  margin-right: 0.5em;
  font-family: 'GT Walsheim Pro Cond';
  font-size: 12px;
  font-weight: 500;
  color: ${colors.main};
  text-align: center;
  text-transform: uppercase;
  border: 2px solid ${colors.main};
  border-radius: 50%;
`
const Venue = styled.div``
const Link = styled.div`
  align-self: center;
  color: ${colors.main};
`
export default function Event(props) {
  const { lang } = useContext(I18nContext)

  moment.locale(lang)

  return (
    <Wrapper href={props.event.acf.billeterie} target='_blank'>
      <Time>{moment(props.event.acf.horaires.debut).format('ddd DD MMM')}</Time>
      <Content>
        <Title
          dangerouslySetInnerHTML={{
            __html: props.event.title.rendered
          }}
        />
        {!props.curatorScreen && props.event.curator && (
          <Curator curator={props.event.curator} />
        )}
        <Description
          dangerouslySetInnerHTML={{
            __html: props.event.acf.description
          }}
        />

        {props.curatorScreen && props.event.acf.lieux && (
          <Venue>{props.event.acf.lieux[0].post_title}</Venue>
        )}
        <Tags element={props.event} />
      </Content>
      <Link>
        <svg width='15.167px' height='21.167px'>
          <path
            stroke='rgb(255, 61, 56)'
            strokeWidth='2.778px'
            strokeLinecap='butt'
            strokeLinejoin='miter'
            fill='none'
            d='M3.879,2.248 L11.726,10.098 L3.879,17.948 '
          />
        </svg>
      </Link>
    </Wrapper>
  )
}
