import React, { useContext } from 'react'
import styled from 'styled-components'

import I18nContext from 'utils/i18nContext'
import Title from 'components/misc/Title'

const Wrapper = styled.div``

const Text = styled.div`
  iframe {
    max-width: 100%;
  }
`
export default function Description(props) {
  const { translate } = useContext(I18nContext)

  return (
    <Wrapper>
      <Title>
        <span>{translate('Description')}</span>
      </Title>
      <Text
        dangerouslySetInnerHTML={{
          __html: props.description
        }}
      />
    </Wrapper>
  )
}
