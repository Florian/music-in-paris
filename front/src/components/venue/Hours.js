import React, { useState, useEffect, useContext } from 'react'
import styled from 'styled-components'
import moment from 'moment'

import I18nContext from 'utils/i18nContext'
import { days, isOpenNow } from 'utils/date'

const Wrapper = styled.div`
  margin: 1em 1em 1.5em;
  font-size: 14px;
`
const Line = styled.div`
  display: flex;
  margin-bottom: 0.7em;
  font-weight: ${props => (props.current ? '900' : 'normal')};
  line-height: 1;
`
const Day = styled.div`
  text-align: right;
  width: 4.5em;
  margin-right: 1.5em;
`
const Hour = styled.div`
  flex: 1;
  display: flex;
  justify-content: space-between;
  border-bottom: 1px solid white;
`
export default function Hours(props) {
  const { translate, lang } = useContext(I18nContext)

  const [state, setState] = useState({ open: false, yesterday: false })

  useEffect(() => {
    setState(isOpenNow(props.hours))
  }, [props.hours])

  return (
    <Wrapper>
      {days.map((day, index) => (
        <Line
          key={index}
          current={
            (state.yesterday ? moment().day() : moment().isoWeekday() - 1) ===
            index
          }
        >
          <Day>{translate(day)}</Day>

          {props.hours[day].ouvert ? (
            props.hours[day].horaires.ouverture &&
            props.hours[day].horaires.fermeture ? (
              <Hour>
                <div>
                  {moment(props.hours[day].horaires.ouverture, 'HH:mm').format(
                    lang === 'en' ? 'h:mmA' : 'HH:mm'
                  )}
                </div>
                <div>
                  {moment(props.hours[day].horaires.fermeture, 'HH:mm').format(
                    lang === 'en' ? 'h:mmA' : 'HH:mm'
                  )}
                </div>
              </Hour>
            ) : (
              translate('Ouvert')
            )
          ) : (
            translate('Fermé')
          )}
        </Line>
      ))}
    </Wrapper>
  )
}
