import React, { useState, useEffect, useContext } from 'react'
import styled from 'styled-components'

import I18nContext from 'utils/i18nContext'
import { isOpenNow } from 'utils/date'

const Wrapper = styled.div`
  cursor: pointer;
  text-decoration: underline;
`

export default function OpenNow(props) {
  const { translate } = useContext(I18nContext)

  const [state, setState] = useState({ open: false, yesterday: false })
  useEffect(() => {
    setState(isOpenNow(props.hours))
  }, [props.hours])

  return (
    <Wrapper interactive={props.full}>
      {translate(state.open ? 'Ouvert' : 'Fermé')} {translate('maintenant')}
    </Wrapper>
  )
}
