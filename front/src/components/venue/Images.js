import React from 'react'
import styled from 'styled-components'
import 'react-responsive-carousel/lib/styles/carousel.min.css'
import { Carousel } from 'react-responsive-carousel'

import { getSrcset } from 'utils/img'

const Wrapper = styled.div`
  margin: 50px 0 1em;
`
const Img = styled.img``
export default function Images(props) {
  return props.images ? (
    <Wrapper>
      <Carousel showIndicators={false} infiniteLoop showThumbs={false}>
        {props.images.map(({ visuel }) => (
          <Img
            key={visuel.id}
            src={visuel.url}
            alt={visuel.alt}
            srcSet={getSrcset(visuel.sizes)}
            size={'100vw'}
          />
        ))}
      </Carousel>
    </Wrapper>
  ) : (
    ''
  )
}
