import React from 'react'
import styled from 'styled-components'

import useWindowSize from 'hooks/useWindowSize'

const Wrapper = styled.a`
  display: flex;
  justify-content: space-between;
  align-items: center;
  text-align: center;
  margin: 0 2em;
`
export default function Address(props) {
  const windowSize = useWindowSize()

  return (
    <Wrapper
      href={
        windowSize.width > 1200
          ? `https://www.google.com/maps/place/${props.address.address}`
          : `geo:${props.address.lat},${props.address.lng}?q=${props.address.lat},${props.address.lng}(${props.address.address})`
      }
      target={windowSize.width > 1200 ? '_blank' : ''}
    >
      <svg width='13px' height='19px'>
        <path
          fill='rgb(255, 255, 255)'
          d='M6.264,18.136 C6.264,18.136 0.375,10.571 0.375,6.999 C0.375,3.426 3.011,0.531 6.264,0.531 C9.516,0.531 12.153,3.426 12.153,6.999 C12.153,10.571 6.264,18.136 6.264,18.136 ZM6.420,4.419 C5.254,4.419 4.308,5.371 4.308,6.545 C4.308,7.719 5.254,8.670 6.420,8.670 C7.587,8.670 8.532,7.719 8.532,6.545 C8.532,5.371 7.587,4.419 6.420,4.419 Z'
        />
      </svg>
      {props.address.address}
      <svg width='14.167px' height='19.167px'>
        <path
          stroke='rgb(255, 61, 56)'
          strokeWidth='2.778px'
          strokeLinecap='butt'
          strokeLinejoin='miter'
          fill='none'
          d='M3.746,1.945 L10.722,8.924 L3.746,15.904 '
        />
      </svg>
    </Wrapper>
  )
}
