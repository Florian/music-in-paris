import React, { useContext } from 'react'
import styled from 'styled-components'

import { colors } from 'utils/styles'
import I18nContext from 'utils/i18nContext'
import Lives from 'components/svg/Lives'
import Clubs from 'components/svg/Clubs'
import Records from 'components/svg/Records'
import Studios from 'components/svg/Studios'

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  margin-right: 0.4em;
  font-weight: 300;
  text-transform: uppercase;
`
const Icon = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 2em;
  width: 2em;
  margin-right: 0.4em;
  border: 1px solid ${colors.main};
  border-radius: 50%;
`
export default function Category(props) {
  const { translate } = useContext(I18nContext)

  return (
    <Wrapper>
      <Icon>
        {props.category === 'live' && <Lives width='80%' height='80%' />}
        {props.category === 'clubs' && <Clubs width='80%' height='80%' />}
        {props.category === 'records' && <Records width='80%' height='80%' />}
        {props.category === 'studios' && <Studios width='80%' height='80%' />}
      </Icon>
      {translate(props.category)}
    </Wrapper>
  )
}
