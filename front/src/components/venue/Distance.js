import React, { useContext } from 'react'
import styled from 'styled-components'

import I18nContext from 'utils/i18nContext'
import useDistanceToAddress from 'hooks/useDistanceToAddress'

const Wrapper = styled.span`
  text-decoration: ${props => (props.interactive ? 'underline' : 'none')};
`
export default function Distance(props) {
  const { translate } = useContext(I18nContext)

  const distance = useDistanceToAddress(props.address)
  console.log(distance)
  return (
    <Wrapper interactive={props.interactive}>
      {distance
        ? distance > 1
          ? Math.round(distance) + 'km'
          : Math.round(distance * 1000) + 'm'
        : translate('Localisation en cours...')}
    </Wrapper>
  )
}
