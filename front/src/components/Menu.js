import React, { useState } from 'react'

import Burger from './menu/Burger'
import PanelMenu from './menu/PanelMenu'

export default function Menu() {
  const [open, setOpen] = useState(false)

  return (
    <>
      <Burger open={open} toggleOpen={() => setOpen(!open)} />
      <PanelMenu open={open} closeMenu={() => setOpen(false)} />
    </>
  )
}
