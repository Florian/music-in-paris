import React from 'react'
import styled from 'styled-components'

import { colors } from 'utils/styles'

const Wrapper = styled.div`
  position: fixed;
  z-index: 150;
  top: 0;
  right: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 50px;
  height: 50px;
  cursor: pointer;
`
const Button = styled.div`
  position: relative;
  width: 20px;
  height: 20px;
`
const Bar = styled.div`
  position: absolute;
  left: 0;
  width: 100%;
  height: ${props => (props.open ? '6px' : '2px')};
  ${props => (props.open && props.top ? 'transform: rotate(45deg);' : '')}
  ${props =>
    props.open && props.bottom
      ? 'transform: rotate(-45deg);'
      : ''}
  transform-origin: left;
  background-color: ${colors.main};
  ${props => (props.open && props.middle ? 'opacity: 0;' : '')}
  transition: all 300ms ease-out;
`
export default function Burger(props) {
  return (
    <Wrapper onClick={props.toggleOpen}>
      <Button>
        <Bar open={props.open} style={{ top: 0 }} top />
        <Bar
          open={props.open}
          style={{ top: '50%', transform: 'translateY(-50%)' }}
          middle
        />
        <Bar open={props.open} style={{ bottom: 0 }} bottom />
      </Button>
    </Wrapper>
  )
}
