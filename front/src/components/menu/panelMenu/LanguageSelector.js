import React, { useContext } from 'react'
import styled from 'styled-components'

import { colors } from 'utils/styles'
import I18nContext from 'utils/i18nContext'

const Wrapper = styled.div`
  display: flex;
  margin-bottom: 2em;
`
const Item = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 2.5em;
  height: 2.5em;
  margin-right: 0.5em;
  color: ${colors.main};
  text-transform: uppercase;
  border-radius: 1.25em;
  border: ${props => (props.active ? '2px' : '0')} solid ${colors.main};
  cursor: pointer;
`
export default function LanguageSelector() {
  const { lang, setLang } = useContext(I18nContext)

  return (
    <Wrapper>
      <Item onClick={() => setLang('fr')} active={lang === 'fr'}>
        FR
      </Item>
      <Item onClick={() => setLang('en')} active={lang === 'en'}>
        EN
      </Item>
    </Wrapper>
  )
}
