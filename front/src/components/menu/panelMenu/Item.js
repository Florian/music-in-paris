import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

const StyledLink = styled(Link)`
  display: block;
  margin-bottom: 2em;
  font-size: 14px;
  text-transform: uppercase;
  text-decoration: none;
`
const Icon = styled.div``
const Text = styled.span``
export default function Item(props) {
  return (
    <StyledLink onClick={props.closeMenu} to={props.to}>
      <Icon></Icon>
      <Text
        dangerouslySetInnerHTML={{
          __html: props.title
        }}
      />
    </StyledLink>
  )
}
