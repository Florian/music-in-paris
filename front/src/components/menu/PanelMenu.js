import React, { useContext } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import { colors, mq } from 'utils/styles'
import ApiContext from 'utils/apiContext'
import I18nContext from 'utils/i18nContext'

import Item from './panelMenu/Item'
import LanguageSelector from './panelMenu/LanguageSelector'

const Wrapper = styled.div`
  position: fixed;
  z-index: 140;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  pointer-events: ${props => (props.open ? 'inherit' : 'none')};
`
const Background = styled.div`
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  pointer-events: ${props => (props.open ? 'inherit' : 'none')};
`
const PanelWrapper = styled.div`
  position: absolute;
  overflow: hidden;
  top: 0;
  right: 0;
  width: 320px;
  height: 100%;
  transform: translateX(${props => (props.open ? '0' : '130%')});
  padding: 20vh 2em 2em;
  background-color: #0c003b;
  background-image: url('/img/menu.png');
  background-position: bottom center;
  background-size: contain;
  background-repeat: no-repeat;
  transition: transform 300ms ease-out;
  box-shadow: -10px 0px 25px 0px rgba(0, 0, 0, 0.6);

  &:before {
    content: '';
    position: absolute;
    top: 2em;
    left: -115px;
    width: 550px;
    height: 550px;
    border: 5px solid ${colors.main};
    border-radius: 50%;
    pointer-events: none;

    ${mq.large} {
      left: -5vw;
      width: 43.33vw;
      height: 43.33vw;
    }
  }

  ${mq.large} {
    width: 33.33vw;
    box-shadow: none;
    opacity: ${props => (props.open ? '1' : '0')};
    transition: opacity 300ms ease-out;
  }
`
const Logo = styled(Link)`
  display: block;
  margin-bottom: 1.5em;
  font-size: 36px;
  font-weight: bold;
  text-align: center;
  text-decoration: none;
  color: ${colors.main};
`
const Small = styled.span`
  font-size: 0.6em;
  font-weight: normal;
  color: ${colors.main};
`
const StyledLink = styled.a`
  display: block;
  margin-bottom: 2em;
  font-size: 14px;
  text-transform: uppercase;
  text-decoration: none;
`
export default function PanelMenu(props) {
  const { pages } = useContext(ApiContext)
  const { translate } = useContext(I18nContext)

  return (
    <Wrapper open={props.open}>
      <Background onClick={props.closeMenu} open={props.open} />
      <PanelWrapper open={props.open}>
        <Logo to='/' onClick={props.closeMenu}>
          Music <Small>in</Small> Paris
        </Logo>
        <StyledLink href={'https://www.reseau-map.fr'} target='_blank'>
          {translate('Toute l’actualité de la musique')}
        </StyledLink>
        {pages &&
          pages.map(page => (
            <Item
              key={page.id}
              closeMenu={props.closeMenu}
              to={'/pages/' + page.slug}
              slug={page.slug}
              title={page.title.rendered}
            />
          ))}
        <LanguageSelector />
      </PanelWrapper>
    </Wrapper>
  )
}
