import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import ReactMapGL, { FlyToInterpolator } from 'react-map-gl'
import 'mapbox-gl/dist/mapbox-gl.css'
import WebMercatorViewport from '@math.gl/web-mercator'

import useWindowSize from 'hooks/useWindowSize'
import Geolocator from './map/Geolocator'

function Map(props) {
  const windowSize = useWindowSize()

  const [viewport, setViewport] = useState(
    new WebMercatorViewport({
      width:
        windowSize.width > 1200 ? (windowSize.width / 3) * 2 : windowSize.width,
      height: windowSize.height
    }).fitBounds([
      [2.2147121649, 48.7881366783],
      [2.4875598769, 48.9426131391]
    ])
  )

  const history = useHistory()

  useEffect(() => {
    if (props.currentCenter) {
      setViewport(prevViewport => ({
        ...prevViewport,
        latitude: props.currentCenter.acf.adresse.lat,
        longitude: props.currentCenter.acf.adresse.lng,
        zoom: 13,
        transitionDuration: 300,
        transitionInterpolator: new FlyToInterpolator()
      }))
    }
  }, [props.currentCenter])

  useEffect(() => {
    setViewport(
      new WebMercatorViewport({
        width:
          windowSize.width > 1200
            ? (windowSize.width / 3) * 2
            : windowSize.width,
        height: windowSize.height
      }).fitBounds([
        [2.2147121649, 48.7881366783],
        [2.4875598769, 48.9426131391]
      ])
    )
  }, [windowSize])

  function onViewportChange(viewport) {
    setViewport(viewport)
  }

  return (
    <ReactMapGL
      {...viewport}
      mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_API_TOKEN}
      mapStyle={'mapbox://styles/pharollpharoll/ck6es3b8f0fo21irr6hjx3fhs'}
      onViewportChange={onViewportChange}
      onInteractionStateChange={interactionState =>
        interactionState.isDragging && props.currentCenter
          ? history.push('/')
          : null
      }
      onNativeClick={() => (props.currentCenter ? history.push('/') : null)}
    >
      <Geolocator
        focusOnUser={({ latitude, longitude }) => {
          setViewport(prevViewport => ({
            ...prevViewport,
            latitude,
            longitude,
            zoom: 13,
            transitionDuration: 300,
            transitionInterpolator: new FlyToInterpolator()
          }))
        }}
      />
      {props.children}
    </ReactMapGL>
  )
}

export default Map
