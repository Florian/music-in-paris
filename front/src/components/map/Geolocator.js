import React, { useState, useEffect } from 'react'
import styled, { keyframes } from 'styled-components'
import { Marker } from 'react-map-gl'

import { colors, mq } from 'utils/styles'

const Button = styled.div`
  position: absolute;
  top: calc(50px + 1em);
  right: 1em;
  padding: 0.2em;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 40px;
  height: 40px;
  background-color: ${colors.background};
  border: 1px solid ${colors.main};

  ${mq.large} {
    top: 1em;
  }
`
const Svg = styled.svg`
  width: 85%;
  height: 85%;
`
const rotate = keyframes`
  0% {
    transform: scale(1) translate(-50%, -50%);
  }
  50% {
    transform: scale(1.5) translate(-50%, -50%);
  }
  100% {
    transform: scale(1) translate(-50%, -50%);
  }
`
const User = styled.div`
  position: relative;
  width: 30px;
  height: 30px;
  border: 2px solid ${colors.main};
  border-radius: 50%;

  &:before {
    content: '';
    position: absolute;
    top: 50%;
    left: 50%;
    transform-origin: left top;
    width: 12px;
    height: 12px;
    background-color: ${colors.main};
    border-radius: 50%;
    animation: ${rotate} 4s linear infinite;
  }
`

export default function Geolocator(props) {
  const [userPosition, setUserPosition] = useState(null)

  useEffect(() => {
    var watchID = navigator.geolocation.watchPosition(position => {
      setUserPosition(position.coords)
    })
    return function cleanup() {
      navigator.geolocation.clearWatch(watchID)
    }
  }, [])

  return userPosition ? (
    <>
      <Button onClick={() => props.focusOnUser(userPosition)}>
        <Svg x='0px' y='0px' viewBox='0 0 198.19 198.7'>
          <path
            style={{ fill: colors.main }}
            d='M95.67,127.73c-15.76,0-28.58-12.82-28.58-28.58s12.82-28.58,28.58-28.58c15.76,0,28.58,12.82,28.58,28.58
           S111.43,127.73,95.67,127.73z M95.67,81.23c-9.88,0-17.92,8.04-17.92,17.92s8.04,17.92,17.92,17.92s17.92-8.04,17.92-17.92
           S105.55,81.23,95.67,81.23z'
          />

          <path
            style={{ fill: colors.main }}
            d='M95.67,168.28c-38.12,0-69.14-31.01-69.14-69.14s31.01-69.14,69.14-69.14s69.14,31.01,69.14,69.14
           S133.79,168.28,95.67,168.28z M95.67,40.68c-32.24,0-58.47,26.23-58.47,58.47s26.23,58.47,58.47,58.47s58.47-26.23,58.47-58.47
           S127.91,40.68,95.67,40.68z'
          />

          <path
            style={{ fill: colors.main }}
            d='M181.32,104.48h-19.49c-2.95,0-5.33-2.39-5.33-5.33s2.39-5.33,5.33-5.33h19.49c2.95,0,5.33,2.39,5.33,5.33
           S184.27,104.48,181.32,104.48z'
          />

          <path
            style={{ fill: colors.main }}
            d='M30.15,104.48H10.67c-2.95,0-5.33-2.39-5.33-5.33s2.39-5.33,5.33-5.33h19.49c2.95,0,5.33,2.39,5.33,5.33
           S33.1,104.48,30.15,104.48z'
          />

          <path
            style={{ fill: colors.main }}
            d='M95.99,38.64c-2.95,0-5.33-2.39-5.33-5.33V13.82c0-2.95,2.39-5.33,5.33-5.33s5.33,2.39,5.33,5.33V33.3
           C101.33,36.25,98.94,38.64,95.99,38.64z'
          />

          <path
            style={{ fill: colors.main }}
            d='M95.99,189.8c-2.95,0-5.33-2.39-5.33-5.33v-19.49c0-2.95,2.39-5.33,5.33-5.33s5.33,2.39,5.33,5.33v19.49
           C101.33,187.42,98.94,189.8,95.99,189.8z'
          />
        </Svg>
      </Button>
      <Marker
        latitude={userPosition.latitude}
        longitude={userPosition.longitude}
      >
        <User />
      </Marker>
    </>
  ) : null
}
