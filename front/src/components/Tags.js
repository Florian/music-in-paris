import React, { useContext } from 'react'
import styled from 'styled-components'

import TermsContext from 'utils/termsContext'
import Tag from './tags/Tag'

const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  ${props => (props.all ? '' : 'max-height: calc(3.8em + 4px)')};
  margin-bottom: ${props => (props.all ? '1rem' : '5px')};
  font-size: 10px;
  overflow: hidden;
`

export default function Tags(props) {
  const { moods, spaces, genres, activities } = useContext(TermsContext)
  return (
    <Wrapper all={props.all}>
      {moods.length && props.element.mood
        ? props.element.mood.map(tag => (
            <Tag key={tag}>
              {moods.find(term => term.id === tag) &&
                moods.find(term => term.id === tag).name}
            </Tag>
          ))
        : ''}
      {spaces.length && props.element.space
        ? props.element.space.map(tag => (
            <Tag key={tag}>
              {spaces.find(term => term.id === tag) &&
                spaces.find(term => term.id === tag).name}
            </Tag>
          ))
        : ''}
      {genres.length && props.element.genre
        ? props.element.genre.map(tag => (
            <Tag key={tag}>
              {genres.find(term => term.id === tag) &&
                genres.find(term => term.id === tag).name}
            </Tag>
          ))
        : ''}
      {activities.length && props.element.activity
        ? props.element.activity.map(tag => (
            <Tag key={tag}>
              {activities.find(term => term.id === tag) &&
                activities.find(term => term.id === tag).name}
            </Tag>
          ))
        : ''}
    </Wrapper>
  )
}
