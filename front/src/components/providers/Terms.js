import React, { useState, useEffect, useContext } from 'react'

import api from 'utils/api'
import I18nContext from 'utils/i18nContext'
import TermsContext from 'utils/termsContext'

export default function Terms(props) {
  const { lang } = useContext(I18nContext)

  const [categories, setCategories] = useState([])
  const [taxonomies, setTaxonomies] = useState([])
  const [moods, setMoods] = useState([])
  const [spaces, setSpaces] = useState([])
  const [genres, setGenres] = useState([])
  const [activities, setActivities] = useState([])

  useEffect(() => {
    api.fetchCategories(lang).then(categories => setCategories(categories))
    api.fetchTaxonomies(lang).then(taxonomies => setTaxonomies(taxonomies))
    api.fetchMoods(lang).then(moods => setMoods(moods))
    api.fetchSpaces(lang).then(spaces => setSpaces(spaces))
    api.fetchGenres(lang).then(genres => setGenres(genres))
    api.fetchActivities(lang).then(activities => setActivities(activities))
  }, [])

  return (
    <TermsContext.Provider
      value={{ categories, taxonomies, moods, spaces, genres, activities }}
    >
      {props.children}
    </TermsContext.Provider>
  )
}
