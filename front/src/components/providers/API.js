import React, { useState, useEffect, useContext } from 'react'
import * as moment from 'moment'

import api from 'utils/api'
import I18nContext from 'utils/i18nContext'
import ApiContext from 'utils/apiContext'

export default function API(props) {
  const { lang } = useContext(I18nContext)

  const [pages, setPages] = useState([])
  const [lives, setLives] = useState([])
  const [clubs, setClubs] = useState([])
  const [studios, setStudios] = useState([])
  const [records, setRecords] = useState([])
  const [events, setEvents] = useState([])
  const [curators, setCurators] = useState([])

  useEffect(() => {
    api.fetchPages(lang).then(pages => setPages(pages))
    api.fetchLives(lang).then(lives => setLives(lives))
    api.fetchClubs(lang).then(clubs => setClubs(clubs))
    api.fetchStudios(lang).then(studios => setStudios(studios))
    api.fetchRecords(lang).then(records => setRecords(records))
    api
      .fetchEvents(lang)
      .then(events =>
        setEvents(
          events.filter(
            event => moment().diff(moment(event.acf.horaires.debut)) < 0
          )
        )
      )
    api.fetchCurators(lang).then(curators => setCurators(curators))
  }, [lang])

  return (
    <ApiContext.Provider
      value={{ pages, lives, clubs, studios, records, events, curators }}
    >
      {props.children}
    </ApiContext.Provider>
  )
}
