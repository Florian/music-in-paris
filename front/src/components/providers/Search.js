import React, { useState } from 'react'

import SearchContext from 'utils/searchContext'

export default function Search(props) {
  const [date, setDate] = useState('today')
  const [category, setCategory] = useState()
  const [moods, setMoods] = useState([])
  const [spaces, setSpaces] = useState([])
  const [genres, setGenres] = useState([])
  const [activities, setActivities] = useState([])

  return (
    <SearchContext.Provider
      value={{
        date,
        setDate,
        category,
        setCategory,
        moods,
        setMoods,
        spaces,
        setSpaces,
        genres,
        setGenres,
        activities,
        setActivities
      }}
    >
      {props.children}
    </SearchContext.Provider>
  )
}
