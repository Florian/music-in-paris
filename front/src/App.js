import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import { GlobalStyle } from 'utils/styles'
import I18n from 'components/providers/I18n'
import API from 'components/providers/API'
import Terms from 'components/providers/Terms'
import Search from 'components/providers/Search'

import Loader from 'components/misc/Loader'
import Menu from 'components/Menu'
import Logo from 'components/misc/Logo'
import Venues from 'views/Venues'
import Curators from 'views/Curators'
import Page from 'views/Page'

export default function App() {
  return (
    <I18n>
      <API>
        <Terms>
          <Search>
            <Router>
              <GlobalStyle />
              <Loader />
              <Menu />
              <Logo />
              <Switch>
                <Route path='/curators/:id?'>
                  <Curators />
                </Route>
                <Route path='/pages/:slug'>
                  <Page />
                </Route>
                <Route path={['/venues/:id?', '/']}>
                  <Venues />
                </Route>
              </Switch>
            </Router>
          </Search>
        </Terms>
      </API>
    </I18n>
  )
}
