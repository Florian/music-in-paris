import { useState, useEffect } from 'react'

export default function useUserLocation() {
  const [userLocation, setUserLocation] = useState(null)

  useEffect(() => {
    if (navigator.geolocation) {
      const watcher = navigator.geolocation.watchPosition(setUserLocation)
      return () => navigator.geolocation.clearWatch(watcher)
    }
  }, [])

  return userLocation
}
