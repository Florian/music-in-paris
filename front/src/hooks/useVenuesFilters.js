import { useState, useEffect, useContext } from 'react'
import * as moment from 'moment'
import 'moment/locale/fr'

import { days, isOpenNow } from 'utils/date'
import SearchContext from 'utils/searchContext'

export default function useVenuesFilters({
  lives,
  clubs,
  studios,
  records,
  events,
  curators,
  currentVenue
}) {
  const search = useContext(SearchContext)

  const [filteredVenues, setFilteredVenues] = useState(
    search.category === 'lives'
      ? lives
      : search.category === 'clubs'
      ? clubs
      : search.category === 'studios'
      ? studios
      : search.category === 'records'
      ? records
      : []
  )

  useEffect(() => {
    let venues =
      search.category === 'lives'
        ? lives
        : search.category === 'clubs'
        ? clubs
        : search.category === 'studios'
        ? studios
        : search.category === 'records'
        ? records
        : []

    for (let curator of curators) {
      for (let eventId of curator.acf['evenements']) {
        const eventObject = events.find(event => event.id === eventId)
        venues = venues.map(venue => {
          if (eventObject && eventObject.acf.lieu[0] === venue.id) {
            venue.curated = true
          }
          return venue
        })
      }
    }
    setFilteredVenues(
      venues
        .filter(venue => {
          if (
            search.moods.length &&
            !search.moods.filter(mood => venue.mood.includes(mood)).length
          ) {
            return false
          }
          if (
            search.spaces.length &&
            !search.spaces.filter(space => venue.space.includes(space)).length
          ) {
            return false
          }
          if (
            search.genres.length &&
            !search.genres.filter(genre => venue.genre.includes(genre)).length
          ) {
            return false
          }
          if (
            search.activities.length &&
            !search.activities.filter(activity =>
              venue.activity.includes(activity)
            ).length
          ) {
            return false
          }
          return true
        })
        .map(venue => {
          venue.current = venue === currentVenue
          return venue
        })
        .map(venue => {
          venue.open = false

          /*if (search.category === 'lives' || search.category === 'clubs') {
            for (let event of events) {
              if (event.acf.lieu[0] === venue.id) {
                if (event.acf.horaires.debut)
                  if (search.date === 'today') {
                    if (
                      moment(event.acf.horaires.debut).dayOfYear() ===
                      moment().dayOfYear()
                    ) {
                      venue.open = true
                    }
                  }
                if (search.date === 'tomorrow') {
                  if (
                    moment(event.acf.horaires.debut).dayOfYear() ===
                    moment().dayOfYear() + 1
                  ) {
                    venue.open = true
                  }
                }
                if (search.date === 'week') {
                  if (
                    moment(event.acf.horaires.debut).week() === moment().week()
                  ) {
                    venue.open = true
                  }
                }

                if (search.date === 'month') {
                  if (
                    moment(event.acf.horaires.debut).month() ===
                    moment().month()
                  ) {
                    venue.open = true
                  }
                }
              }
            }
          } else {*/
          venue.open =
            search.day || search.day === 0
              ? venue.acf.horaires[days[search.day]].ouvert
              : isOpenNow(venue.acf.horaires).open
          //}
          return venue
        })
    )
  }, [search, lives, clubs, studios, records, events, curators, currentVenue])

  return filteredVenues
}
