import { useState, useEffect } from 'react'

export default function useDistanceToAddress(address) {
  const [userLocation, setUserLocation] = useState(null)

  const [mounted, setMounted] = useState(true)

  useEffect(() => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        if (mounted) {
          setUserLocation(position)
        }
      })

      return () => setMounted(false)
    }
  }, [mounted])

  const [distance, setDistance] = useState(null)

  useEffect(() => {
    if (userLocation && address && mounted) {
      const R = 6371 // Radius of the earth in km
      const dLat = deg2rad(address.lat - userLocation.coords.latitude)
      const dLon = deg2rad(address.lng - userLocation.coords.longitude)
      const a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(userLocation.coords.latitude)) *
          Math.cos(deg2rad(address.lat)) *
          Math.sin(dLon / 2) *
          Math.sin(dLon / 2)
      const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
      const d = R * c
      setDistance(d)
    }
    return () => setMounted(false)
  }, [userLocation, address, mounted])

  return distance
}

function deg2rad(deg) {
  return deg * (Math.PI / 180)
}
