export function getSrcset(sizes) {
  if (sizes) {
    return `
	    ${sizes['1920']} ${sizes['1920-width']}w,
		${sizes['1280']} ${sizes['1280-width']}w,
		${sizes['960']} ${sizes['960-width']}w,
		${sizes['768']} ${sizes['768-width']}w,
		${sizes['375']} ${sizes['375-width']}w,
		${sizes['170']} ${sizes['170-width']}w,
		${sizes['placeholder']} ${sizes['placeholder-width']}w,
		${sizes['medium']} ${sizes['medium-width']}w,
		${sizes['large']} ${sizes['large-width']}w`
  }
}
