import React from 'react'

export default React.createContext({
  categories: null,
  taxonomies: null,
  moods: null,
  spaces: null,
  genres: null,
  activities: null
})
