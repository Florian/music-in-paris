const wp = process.env.REACT_APP_API + '/'

const pages = wp + 'wp-json/wp/v2/pages'
const venues = wp + 'wp-json/wp/v2/venues'
const lives = wp + 'wp-json/wp/v2/live'
const clubs = wp + 'wp-json/wp/v2/clubs'
const studios = wp + 'wp-json/wp/v2/studios'
const records = wp + 'wp-json/wp/v2/records'
const curators = wp + 'wp-json/wp/v2/curators'
const events = wp + 'wp-json/wp/v2/events'
const categories = wp + 'wp-json/wp/v2/categories'
const moods = wp + 'wp-json/wp/v2/mood'
const spaces = wp + 'wp-json/wp/v2/space'
const genres = wp + 'wp-json/wp/v2/genre'
const activities = wp + 'wp-json/wp/v2/activity'
const taxonomies = wp + 'wp-json/wp/v2/taxonomies'

export default {
  handleErrors(response) {
    if (!response.ok) {
      throw Error(response.statusText)
    }
    return response
  },
  get(endpoint, lang) {
    let headers = new Headers()
    return fetch(
      endpoint +
        '?per_page=100' +
        (lang && lang !== 'fr' ? `&lang=${lang}` : ''),
      {
        method: 'GET',
        headers: headers
      }
    )
      .then(this.handleErrors)
      .then(res => res.json())
  },
  fetchPages(lang) {
    return this.get(pages, lang).then(
      result => result,
      error => {
        console.log(error)
      }
    )
  },
  fetchVenues(lang) {
    return this.get(venues, lang).then(
      result => result,
      error => {
        console.log(error)
      }
    )
  },
  fetchLives(lang) {
    return this.get(lives, lang).then(
      result => result,
      error => {
        console.log(error)
      }
    )
  },
  fetchClubs(lang) {
    return this.get(clubs, lang).then(
      result => result,
      error => {
        console.log(error)
      }
    )
  },
  fetchStudios(lang) {
    return this.get(studios, lang).then(
      result => result,
      error => {
        console.log(error)
      }
    )
  },
  fetchRecords(lang) {
    return this.get(records, lang).then(
      result => result,
      error => {
        console.log(error)
      }
    )
  },
  fetchCurators(lang) {
    return this.get(curators, lang).then(
      result => result,
      error => {
        console.log(error)
      }
    )
  },
  fetchCategories(lang) {
    return this.get(categories, lang).then(
      result => result,
      error => {
        console.log(error)
      }
    )
  },
  fetchMoods(lang) {
    return this.get(moods, lang).then(
      result => result,
      error => {
        console.log(error)
      }
    )
  },
  fetchSpaces(lang) {
    return this.get(spaces, lang).then(
      result => result,
      error => {
        console.log(error)
      }
    )
  },
  fetchGenres(lang) {
    return this.get(genres, lang).then(
      result => result,
      error => {
        console.log(error)
      }
    )
  },
  fetchActivities(lang) {
    return this.get(activities, lang).then(
      result => result,
      error => {
        console.log(error)
      }
    )
  },
  fetchTaxonomies(lang) {
    return this.get(taxonomies, lang).then(
      result => result,
      error => {
        console.log(error)
      }
    )
  },
  fetchEvents(lang) {
    return this.get(events, lang).then(
      result => result,
      error => {
        console.log(error)
      }
    )
  }
}
