import moment from 'moment'

export const days = [
  'lundi',
  'mardi',
  'mercredi',
  'jeudi',
  'vendredi',
  'samedi',
  'dimanche'
]

export function isOpenNow(hours, date) {
  const dateSearched = moment(date ? date : undefined)

  const currentHours = hours[days[dateSearched.isoWeekday() - 1]]

  const hourOpening = moment(currentHours.horaires.ouverture, 'HH:mm')
  const currentOpening = moment(dateSearched.format())
    .hours(hourOpening.hours())
    .minutes(hourOpening.minutes())

  const hourClosing = moment(currentHours.horaires.fermeture, 'HH:mm')
  const currentClosing = moment(dateSearched.format())
    .hours(hourClosing.hours())
    .minutes(hourClosing.minutes())

  // If not open at all today
  if (!currentHours.ouvert) {
    //console.log('If not open at all today')
    return checkYesterdayHours(hours, dateSearched)
  }

  // If no hours precised
  if (!currentHours.horaires.ouverture && !currentHours.horaires.fermeture) {
    return { open: true }
  }

  // If still not open today
  if (currentOpening.diff(dateSearched) > 0) {
    //console.log('If still not open today')
    return checkYesterdayHours(hours, dateSearched)
  }

  // If today closing time is tomorrow
  if (currentClosing.diff(currentOpening) < 0) {
    //console.log('If today closing time is tomorrow')
    return { open: true }
  }

  // If today closing time is after now
  if (currentClosing.diff(dateSearched) > 0) {
    //console.log('If today closing time is after now')
    return { open: true }
  }
  //console.log('None of the above')
  return { open: false }
}

export function checkYesterdayHours(hours, dateSearched) {
  const yesterdayHours = hours[days[dateSearched.day()]]

  const hourOpening = moment(yesterdayHours.horaires.ouverture, 'HH:mm')
  const currentOpening = moment(dateSearched.format())
    .hours(hourOpening.hours())
    .minutes(hourOpening.minutes())

  const hourClosing = moment(yesterdayHours.horaires.fermeture, 'HH:mm')
  const currentClosing = moment(dateSearched.format())
    .hours(hourClosing.hours())
    .minutes(hourClosing.minutes())

  // If yesterday closing time is today
  if (currentClosing.diff(currentOpening) < 0) {
    // If yesterday closing time is after now
    if (currentClosing.diff(dateSearched) > 0) {
      //console.log('If yesterday closing time is after now')
      return { open: true, yesterday: true }
    }
  }
  //console.log('If yesterday closing time is today')
  return { open: false }
}
