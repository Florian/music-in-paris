import { createGlobalStyle } from 'styled-components'
import styledNormalize from 'styled-normalize'

import fonts from './fonts'

export const colors = {
  main: '#ff3d38',
  secondary: '#2b45c0',
  background: '#080a32',
  text: '#ffffff',
  curators: '#ff3d38',
  white: '#ffffff'
}
export const mq = {
  medium: '@media screen and (min-width: 640px)',
  large: '@media screen and (min-width: 1200px)',
  xlarge: '@media screen and (min-width: 2000px)'
}

export const GlobalStyle = createGlobalStyle`
  ${styledNormalize}
  
  ${fonts}

  html {
    box-sizing: border-box;
    font-family: 'GT Walsheim Pro', Arial, sans-serif;
  }

  *, *:before, *:after {
    margin-top: 0;
    box-sizing: inherit;
    color: ${colors.text};
  }

  body {
    background-color: ${colors.background};
  }

  p {
    font-size: 15px;
  }

  h1, h2, h3, h4, h5, h6 {
    color: ${colors.main};
    margin-bottom: 0.2em;
  }

  h1 {
    font-size: 20px;
  }

  .carousel .slide {
    display: flex;
    justify-content: center;
    background-color: ${colors.background};
}
`
