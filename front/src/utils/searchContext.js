import React from 'react'

export default React.createContext({
  date: null,
  setdate: () => '',
  category: null,
  setCategory: () => {},
  moods: null,
  setMoods: () => {},
  spaces: null,
  setSpaces: () => {},
  genres: null,
  setGenres: () => {},
  activities: null,
  setActivities: () => {}
})
