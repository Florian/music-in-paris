import React from 'react'

export default React.createContext({
  pages: null,
  setPages: () => {},
  lives: null,
  setLives: () => {},
  clubs: null,
  setClubs: () => {},
  studios: null,
  setStudios: () => {},
  records: null,
  setRecords: () => {},
  events: null,
  setEvents: () => {},
  curators: null,
  setCurators: () => {}
})
