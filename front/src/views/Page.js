import React, { useState, useEffect, useContext } from 'react'
import styled from 'styled-components'
import { useParams } from 'react-router-dom'

import useWindowHeight from 'hooks/useWindowHeight'
import { colors } from 'utils/styles'
import ApiContext from 'utils/apiContext'

const Wrapper = styled.div`
  min-height: ${props => props.windowHeight}px;
  padding: 5em 1em;
  background-color: ${colors.background};
`
export default function Page(props) {
  const { pages } = useContext(ApiContext)

  const [currentPage, setCurrentPage] = useState()
  const { slug } = useParams()
  useEffect(() => {
    setCurrentPage(pages.find(page => page.slug === slug))
  }, [pages, slug])

  const windowHeight = useWindowHeight()

  return (
    <Wrapper windowHeight={windowHeight}>
      {currentPage && (
        <h1
          dangerouslySetInnerHTML={{
            __html: currentPage.title.rendered
          }}
        />
      )}
    </Wrapper>
  )
}
