import React, { useState, useEffect, useContext } from 'react'
import styled from 'styled-components'
import { useParams } from 'react-router-dom'

import useWindowHeight from 'hooks/useWindowHeight'
import SearchContext from 'utils/searchContext'
import ApiContext from 'utils/apiContext'

import VenuesMap from './venues/VenuesMap'
import Venue from './venues/Venue'
import CuratorsButton from './venues/CuratorsButton'
import CategorySelector from './venues/CategorySelector'

const Wrapper = styled.div`
  position: relative;
  height: ${props => props.windowHeight}px;
  overflow: hidden;
`
export default function Venues() {
  const { id } = useParams()

  const { lives, clubs, studios, records, curators, events } = useContext(
    ApiContext
  )
  const [currentVenue, setCurrentVenue] = useState(null)

  const { setCategory } = useContext(SearchContext)

  useEffect(() => {
    let newCurrentVenue =
      lives.find(venue => Number(venue.id) === Number(id)) ||
      clubs.find(venue => Number(venue.id) === Number(id)) ||
      studios.find(venue => Number(venue.id) === Number(id)) ||
      records.find(venue => Number(venue.id) === Number(id))

    if (events && curators && newCurrentVenue) {
      for (let curator of curators) {
        for (let eventId of curator.acf['evenements']) {
          const eventObject = events.find(event => event.id === eventId)
          if (eventObject && eventObject.acf.lieu[0] === newCurrentVenue.id) {
            newCurrentVenue.curated = true
          }
        }
      }
    }

    setCurrentVenue(newCurrentVenue)
  }, [lives, clubs, studios, records, curators, events, id])

  useEffect(() => {
    if (currentVenue) {
      setCategory(currentVenue.type === 'live' ? 'lives' : currentVenue.type)
    }
  }, [currentVenue, setCategory])

  const windowHeight = useWindowHeight()

  return (
    <Wrapper windowHeight={windowHeight}>
      <VenuesMap
        lives={lives}
        clubs={clubs}
        studios={studios}
        records={records}
        curators={curators}
        events={events}
        currentVenue={currentVenue}
      />
      {id && (
        <Venue
          events={events}
          curators={curators}
          currentVenue={currentVenue}
        />
      )}
      <CategorySelector />
      <CuratorsButton show={!currentVenue} />
    </Wrapper>
  )
}
