import React, { useState, useEffect, useContext } from 'react'
import styled from 'styled-components'
import { useHistory } from 'react-router-dom'

import { colors } from 'utils/styles'
import useWindowSize from 'hooks/useWindowSize'
import SearchContext from 'utils/searchContext'

import VerticalDrawer from 'components/misc/VerticalDrawer'
import ShortInfos from './venue/ShortInfos'
import FullInfos from './venue/FullInfos'
import Loader from 'components/misc/Loader'

const DesktopWrapper = styled.div`
  position: absolute;
  z-index: 250;
  top: 0;
  right: 0;
  width: 33.33vw;
  height: 100vh;
  overflow-y: scroll;
  background-color: ${colors.background};
`
export default function Venue(props) {
  const { category } = useContext(SearchContext)

  const [open, setOpen] = useState(category ? false : true)

  const windowSize = useWindowSize()

  useEffect(() => {
    if (!props.currentVenue && category) {
      setOpen(false)
    }
  }, [props.currentVenue, category])

  const history = useHistory()

  return windowSize.width > 1200 ? (
    props.currentVenue ? (
      <DesktopWrapper>
        <FullInfos
          venue={props.currentVenue}
          events={props.events}
          curators={props.curators}
        />
      </DesktopWrapper>
    ) : (
      <Loader />
    )
  ) : (
    <VerticalDrawer
      show={props.currentVenue}
      open={open}
      openDrawer={() => setOpen(true)}
      closeDrawer={() => history.push('/')}
    >
      {props.currentVenue ? (
        open ? (
          <FullInfos
            venue={props.currentVenue}
            events={props.events}
            curators={props.curators}
          />
        ) : (
          <ShortInfos venue={props.currentVenue} />
        )
      ) : (
        <Loader />
      )}
    </VerticalDrawer>
  )
}
