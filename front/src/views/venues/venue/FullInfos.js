import React, { useState } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import { colors, mq } from 'utils/styles'
import OpenNow from 'components/venue/OpenNow'
import Hours from 'components/venue/Hours'
import Images from 'components/venue/Images'
import Social from 'components/venue/Social'
import Category from 'components/venue/Category'
import Distance from 'components/venue/Distance'
import Address from 'components/venue/Address'
import Description from 'components/venue/Description'
import Tags from 'components/Tags'
import Events from 'components/Events'
import BackButton from 'components/misc/BackButton'

const Wrapper = styled.div`
  position: relative;
`
const Header = styled.div`
  position: fixed;
  z-index: 6;
  top: 0;
  left: 0;
  right: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 50px;
  background-color: ${colors.background};

  a {
    text-decoration: none;
  }

  ${mq.large} {
    position: relative;
  }
`
const Title = styled.h1`
  margin: 0 50px;
  text-align: center;
`
const Content = styled.div`
  padding: 1em;
`
const Flex = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
`
const Informations = styled.div`
  margin-bottom: 1em;
`

export default function FullInfos(props) {
  const [hoursOpen, setHoursOpen] = useState(false)

  return (
    <Wrapper>
      <Header>
        <Link to='/'>
          <BackButton />
        </Link>
        <Title
          dangerouslySetInnerHTML={{
            __html: props.venue.title.rendered
          }}
        />
      </Header>
      <Images images={props.venue.acf.visuels} />
      <Content>
        <Flex>
          <Category category={props.venue.type} />
          <Social
            links={props.venue.acf.reseaux_sociaux[0]}
            website={props.venue.acf.site_web}
          />
        </Flex>
        <Informations>
          <Flex onClick={() => setHoursOpen(!hoursOpen)}>
            <OpenNow hours={props.venue.acf.horaires} />
            <Distance address={props.venue.acf.adresse} interactive />
          </Flex>
          {hoursOpen && (
            <div>
              <Hours hours={props.venue.acf.horaires} />
              <Address address={props.venue.acf.adresse} />
            </div>
          )}
        </Informations>
        <Description description={props.venue.acf.description} />
        <Tags element={props.venue} all />
        <Events
          events={props.events}
          curators={props.curators}
          venue={props.venue}
        />
      </Content>
    </Wrapper>
  )
}
