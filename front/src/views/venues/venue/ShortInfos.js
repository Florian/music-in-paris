import React from 'react'
import styled from 'styled-components'

import { colors, mq } from 'utils/styles'
import { getSrcset } from 'utils/img'
import OpenNow from 'components/venue/OpenNow'
import Distance from 'components/venue/Distance'
import Tags from 'components/Tags'
import Curated from 'components/svg/Curated'

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  margin: 4vw;

  ${mq.medium} {
    margin: 25px;
  }
`
const Img = styled.img`
  height: 32vw;
  width: 32vw;
  object-fit: cover;
  border: 2px solid ${colors.main};
  border-radius: 50%;

  ${mq.medium} {
    width: 200px;
    height: 200px;
  }
`
const Informations = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: calc(100% - 115px);
  padding: 0.5em;

  ${mq.medium} {
    padding: 1em;
  }
`
const Title = styled.h2`
  display: flex;
  align-items: center;
  font-size: 5.5vw;

  span {
    margin-right: 0.4em;
  }

  ${mq.medium} {
    font-size: 40px;
  }
`
const Details = styled.div`
  display: flex;
  justify-content: space-between;
  font-size: 12px;
`
const Dot = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 0.7em;
  height: 0.7em;
  background-color: ${colors.main};
  border-radius: 50%;
`
export default function ShortInfos(props) {
  return (
    <Wrapper>
      {props.venue.acf.visuels && (
        <Img
          key={props.venue.acf.visuels[0].visuel.id}
          src={props.venue.acf.visuels[0].visuel.url}
          alt={props.venue.acf.visuels[0].visuel.alt}
          srcSet={getSrcset(props.venue.acf.visuels[0].visuel.sizes)}
          size={'115px'}
        />
      )}
      <Informations>
        <div>
          <Title>
            <span
              dangerouslySetInnerHTML={{
                __html: props.venue.title.rendered
              }}
            />
            {props.venue.curated && (
              <Dot open={props.venue.open}>
                <Curated colors={colors.background} width='80%' height='80%' />
              </Dot>
            )}
          </Title>
          <Tags element={props.venue} />
        </div>
        <Details>
          <Distance address={props.venue.acf.adresse} />
          <OpenNow hours={props.venue.acf.horaires} />
        </Details>
      </Informations>
    </Wrapper>
  )
}
