import React, { useContext } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import { colors, mq } from 'utils/styles'
import I18nContext from 'utils/i18nContext'
import Curated from 'components/svg/Curated'

const Wrapper = styled.div`
  position: fixed;
  bottom: 2em;
  left: 50%;
  transform: translateX(-50%);
  display: flex;
  justify-content: center;
  align-items: center;
  width: 310px;
  padding: 0.7em;
  font-size: 14px;
  text-align: center;
  text-transform: uppercase;
  background-color: ${colors.background};
  border: 1px solid ${colors.main};
  cursor: pointer;
  opacity: ${props => (props.show ? 1 : 0)};
  pointer-events: ${props => (props.show ? 'inherit' : 'none')};
  transition: opacity 300ms ease-out;

  span {
    margin-right: 1em;
  }

  ${mq.large} {
    left: 33.33%;
    opacity: 1;
    pointer-events: inherit;
  }
`
export default function CuratorsButton(props) {
  const { translate } = useContext(I18nContext)

  return (
    <Link to='/curators'>
      <Wrapper show={props.show}>
        <span>{translate('Coups de coeur des curateurs')}</span>
        <Curated width='2em' height='2em' />
      </Wrapper>
    </Link>
  )
}
