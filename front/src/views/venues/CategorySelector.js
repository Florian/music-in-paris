import React, { useContext } from 'react'
import styled from 'styled-components'

import { colors, mq } from 'utils/styles'
import I18nContext from 'utils/i18nContext'
import SearchContext from 'utils/searchContext'

import Category from './categorySelector/Category'

const Wrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  padding: 13vw 13vw 17vw;
  background-color: rgba(8, 10, 50, 0.6);

  ${mq.large} {
    z-index: 200;
  }
`
const Title = styled.h2`
  font-size: 8.5vw;

  ${mq.medium} {
    font-size: 40px;
  }
`
const Subtitle = styled.p`
  margin-bottom: 1em;
  ${mq.medium} {
    margin-bottom: 2em;
  }
`
const List = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;

  ${mq.medium} {
    justify-content: space-around;
  }
`
const Logo = styled.h1`
  display: none;
  font-size: 120px;

  ${mq.large} {
    display: block;
  }
`
const Small = styled.span`
  font-size: 0.6em;
  font-weight: normal;
  color: ${colors.main};
`
export default function CategorySelector() {
  const { translate } = useContext(I18nContext)

  const search = useContext(SearchContext)

  return !search.category ? (
    <Wrapper>
      <Logo>
        Music <Small>in</Small> Paris
      </Logo>
      <Title>{translate('Alors, où est ce qu’on sort ?')}</Title>
      <Subtitle>
        {translate(
          'Selectionnez un type d’etablissement pour lancer une recherche instantanée.'
        )}
      </Subtitle>
      <List>
        <Category
          slug={'lives'}
          currentCategory={search.category}
          setCategory={category => {
            search.setCategory(category)
            search.setMoods([])
            search.setSpaces([])
            search.setGenres([])
            search.setActivities([])
          }}
        />
        <Category
          slug={'clubs'}
          currentCategory={search.category}
          setCategory={category => {
            search.setCategory(category)
            search.setMoods([])
            search.setSpaces([])
            search.setGenres([])
            search.setActivities([])
          }}
        />
        <Category
          slug={'records'}
          currentCategory={search.category}
          setCategory={category => {
            search.setCategory(category)
            search.setMoods([])
            search.setSpaces([])
            search.setGenres([])
            search.setActivities([])
          }}
        />
        <Category
          slug={'studios'}
          currentCategory={search.category}
          setCategory={category => {
            search.setCategory(category)
            search.setMoods([])
            search.setSpaces([])
            search.setGenres([])
            search.setActivities([])
          }}
        />
      </List>
    </Wrapper>
  ) : (
    ''
  )
}
