import React, { useState, useContext } from 'react'

import SearchContext from 'utils/searchContext'
import ButtonFilters from './filters/ButtonFilters'
import PanelFilters from './filters/PanelFilters'

export default function Filters() {
  const [open, setOpen] = useState(false)
  const { category } = useContext(SearchContext)
  return (
    <>
      {category && (
        <ButtonFilters
          open={open}
          toggleOpen={() => setOpen(prevOpen => !prevOpen)}
        />
      )}
      <PanelFilters
        open={open}
        toggleOpen={() => setOpen(prevOpen => !prevOpen)}
      />
    </>
  )
}
