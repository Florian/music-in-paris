import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { Marker } from 'react-map-gl'

import { colors } from 'utils/styles'
import Lives from 'components/svg/Lives'
import Clubs from 'components/svg/Clubs'
import Records from 'components/svg/Records'
import Studios from 'components/svg/Studios'
import Curated from 'components/svg/Curated'

const Wrapper = styled.div`
  position: absolute;
  bottom: 0;
  left: 50%;
  transform: translate(-50%, ${props => (props.current ? '0' : '50%')});
  transition: transform 300ms ease-out;
`
const Icon = styled.div`
  position: relative;
  z-index: 10;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 40px;
  height: 40px;
  color: ${props => (props.open ? colors.main : colors.secondary)};
  background-color: ${colors.background};
  border: ${props => (props.current ? '3px' : '2px')} solid
    ${props => (props.open ? colors.main : colors.secondary)};
  border-radius: 50%;
  cursor: pointer;
`
const Current = styled.div`
  position: absolute;
  top: 87%;
  left: 50%;
  transform: translateX(-50%);
  width: 0;
  height: 0;
  border-style: solid;
  border-width: 12px 12px 0 12px;
  border-color: ${props => (props.open ? colors.main : colors.secondary)}
    transparent transparent transparent;

  transition: all 300ms ease-out;
`
const Dot = styled.div`
  position: absolute;
  z-index: 12;
  top: 0;
  right: 0;
  transform: translate(25%, -25%);
  display: flex;
  justify-content: center;
  align-items: center;
  width: 15px;
  height: 15px;
  background-color: ${props => (props.open ? colors.main : colors.secondary)};
  border-radius: 50%;
`

export default function VenueMarker(props) {
  return (
    <Link to={'/venues/' + props.venue.id}>
      <Marker
        latitude={props.venue.acf.adresse.lat}
        longitude={props.venue.acf.adresse.lng}
      >
        <Wrapper current={props.venue.current}>
          <Icon open={props.venue.open} current={props.venue.current}>
            {props.venue.type === 'live' && (
              <Lives
                colors={props.venue.open ? colors.main : colors.secondary}
                width='85%'
                height='85%'
              />
            )}
            {props.venue.type === 'clubs' && (
              <Clubs
                colors={props.venue.open ? colors.main : colors.secondary}
                width='85%'
                height='85%'
              />
            )}
            {props.venue.type === 'records' && (
              <Records
                colors={props.venue.open ? colors.main : colors.secondary}
                width='85%'
                height='85%'
              />
            )}
            {props.venue.type === 'studios' && (
              <Studios
                colors={props.venue.open ? colors.main : colors.secondary}
                width='85%'
                height='85%'
              />
            )}
          </Icon>
          {props.venue.curated && (
            <Dot open={props.venue.open}>
              <Curated colors={colors.background} width='80%' height='80%' />
            </Dot>
          )}
          {props.venue.current && <Current open={props.venue.open} />}
        </Wrapper>
      </Marker>
    </Link>
  )
}
