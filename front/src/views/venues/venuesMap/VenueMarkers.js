import React from 'react'

import VenueMarker from './venueMarkers/VenueMarker'

export default function VenueMarkers(props) {
  return (
    <>
      {props.venues
        .filter(venue => venue.acf.adresse)
        .map(venue => (
          <VenueMarker key={venue.id} venue={venue} events={props.events} />
        ))}
    </>
  )
}
