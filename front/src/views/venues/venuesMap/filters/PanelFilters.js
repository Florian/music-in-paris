import React, { useEffect, useContext, useRef } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import { colors, mq } from 'utils/styles'
import I18nContext from 'utils/i18nContext'
import SearchContext from 'utils/searchContext'

import DateFilters from './panelFilters/DateFilters'
import Categories from './panelFilters/Categories'
import TagsWrapper from './panelFilters/TagsWrapper'
import BackButton from 'components/misc/BackButton'

const Wrapper = styled.div`
  position: fixed;
  z-index: 120;
  top: 0;
  left: 0;
  display: flex;
  flex-direction: column;
  transform: translateX(${props => (props.open ? '0' : '-100%')});
  width: 100%;
  height: 100%;
  background-color: ${colors.background};
  transition: transform 300ms ease-out;

  ${mq.large} {
    position: relative;
    flex: 1;
    height: 100vh;
    transform: none;
    box-shadow: -10px 0 25px 0px rgba(0, 0, 0, 0.6);
    filter: blur(${props => (props.currentCategory ? 0 : '10px')});
  }
`
const BackButtonWrapper = styled.div`
  ${mq.large} {
    display: none;
  }
`
const Header = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 50px;
`
const Title = styled.h1`
  margin: 0;
  ${mq.large} {
    display: none;
  }
`
const Logo = styled.div`
  display: none;

  a {
    text-decoration: none;
  }

  ${mq.large} {
    display: block;
  }
`
const Text = styled.h1`
  margin: 0;
`
const Small = styled.span`
  font-size: 0.6em;
  font-weight: normal;
  color: ${colors.main};
`
const Content = styled.div`
  flex: 1;
  overflow-y: scroll;
  padding: 1em 1em;
`
const ButtonSubmit = styled.div`
  position: fixed;
  bottom: 0;
  left: 0;
  width: 100%;
  padding: 1em;
  font-weight: bold;
  text-transform: uppercase;
  text-align: center;
  color: ${colors.white};
  background-color: ${colors.main};

  ${mq.large} {
    display: none;
  }
`
export default function PanelFilters(props) {
  const { translate } = useContext(I18nContext)

  const content = useRef(null)
  useEffect(() => {
    content.current.scroll(0, 0)
  }, [props.open])

  const search = useContext(SearchContext)

  return (
    <Wrapper open={props.open} currentCategory={search.category}>
      <BackButtonWrapper>
        <BackButton onClick={props.toggleOpen} />
      </BackButtonWrapper>
      <Header>
        <Title>{translate('Filtres de Recherche')}</Title>
        <Logo
          onClick={() => {
            search.setCategory()
            search.setMoods([])
            search.setSpaces([])
            search.setGenres([])
            search.setActivities([])
          }}
        >
          <Link to='/'>
            <Text>
              Music <Small>in</Small> Paris
            </Text>
          </Link>
        </Logo>
      </Header>
      <Content ref={content}>
        <DateFilters />
        <Categories />
        <TagsWrapper />
      </Content>
      <ButtonSubmit onClick={props.toggleOpen}>
        {translate('Valider')}
      </ButtonSubmit>
    </Wrapper>
  )
}
