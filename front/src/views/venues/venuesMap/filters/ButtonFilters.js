import React from 'react'
import styled from 'styled-components'

import { colors, mq } from 'utils/styles'

const Wrapper = styled.div`
  position: fixed;
  z-index: 3;
  top: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 50px;
  height: 50px;
  cursor: pointer;

  &:before {
    content: '';
    position: absolute;
    top: 50%;
    right: 0;
    transform: translateY(-50%);
    width: 1px;
    height: 70%;
    background-color: #08175f;
  }

  ${mq.large} {
    display: none;
  }
`
const Svg = styled.svg`
  height: 26px;
`
export default function ButtonFilters(props) {
  return (
    <Wrapper open={props.open} onClick={props.toggleOpen}>
      <Svg x='0px' y='0px' viewBox='0 0 198.19 198.7'>
        <path
          style={{ fill: colors.main }}
          d='M128.84,128.85c-32.97,0-59.79-26.82-59.79-59.79c0-32.97,26.82-59.79,59.79-59.79
         c32.97,0,59.79,26.82,59.79,59.79C188.63,102.03,161.81,128.85,128.84,128.85z M128.84,19.93c-27.09,0-49.13,22.04-49.13,49.13
         s22.04,49.13,49.13,49.13s49.13-22.04,49.13-49.13S155.93,19.93,128.84,19.93z'
        />

        <path
          style={{ fill: colors.main }}
          d='M70.15,150.14c-1.36,0-2.73-0.52-3.77-1.56l-18.05-18.05c-2.08-2.08-2.08-5.46,0-7.54
         c2.08-2.08,5.46-2.08,7.54,0l18.05,18.05c2.08,2.08,2.08,5.46,0,7.54C72.88,149.62,71.52,150.14,70.15,150.14z'
        />

        <path
          style={{ fill: colors.main }}
          d='M78,124.33c-1.36,0-2.73-0.52-3.77-1.56c-2.08-2.08-2.08-5.46,0-7.54l7.72-7.72c2.08-2.08,5.46-2.08,7.54,0
         c2.08,2.08,2.08,5.46,0,7.54l-7.72,7.72C80.73,123.81,79.36,124.33,78,124.33z'
        />

        <path
          style={{ fill: colors.main }}
          d='M33.65,186.04c-4.55,0-8.82-1.77-12.04-4.99l-4.9-4.91c-3.22-3.21-4.99-7.49-4.99-12.04
         c0-4.55,1.77-8.82,4.99-12.04l38.31-38.31c6.63-6.63,17.43-6.63,24.08,0l4.91,4.91c6.64,6.64,6.64,17.44,0,24.08l-38.31,38.31
         C42.47,184.27,38.2,186.04,33.65,186.04z M67.06,119.44c-1.63,0-3.26,0.62-4.5,1.86l-38.31,38.31c-1.2,1.2-1.86,2.8-1.86,4.5
         c0,1.7,0.66,3.29,1.86,4.49l4.91,4.91c2.41,2.4,6.59,2.4,8.99,0l38.31-38.31c2.48-2.48,2.48-6.52,0-9l-4.91-4.91
         C70.31,120.06,68.68,119.44,67.06,119.44z'
        />
      </Svg>
    </Wrapper>
  )
}
