import React, { useContext } from 'react'
import styled from 'styled-components'

import I18nContext from 'utils/i18nContext'
import SearchContext from 'utils/searchContext'
import { colors, mq } from 'utils/styles'

const Wrapper = styled.div`
  margin-bottom: 0.5em;
  padding: 0 2.2em 1em;
`
const Title = styled.h3`
  font-size: 6.5vw;

  ${mq.medium} {
    font-size: 40px;
  }
`
const Selector = styled.div`
  padding: 0.5em;
`
const Option = styled.div`
  width: 15em;
  padding: 0.4em 1em;
  font-size: 14px;
  color: ${props => (props.active ? colors.background : colors.white)};
  text-transform: uppercase;
  background-color: ${props =>
    props.active ? colors.main : colors.background};
  border-radius: 1em;
  cursor: pointer;
`
export default function DateFilters() {
  const { translate } = useContext(I18nContext)

  const { date, setDate } = useContext(SearchContext)

  return (
    <Wrapper>
      <Title
        dangerouslySetInnerHTML={{
          __html: translate(`Quand<br/>est-ce qu'on sort ?`)
        }}
      />
      <Selector>
        <Option onClick={() => setDate('today')} active={date === 'today'}>
          {translate(`Aujourd'hui`)}
        </Option>
        <Option
          onClick={() => setDate('tomorrow')}
          active={date === 'tomorrow'}
        >
          {translate(`Demain`)}
        </Option>
        <Option onClick={() => setDate('week')} active={date === 'week'}>
          {translate(`Weekend prochain`)}
        </Option>
        <Option onClick={() => setDate('month')} active={date === 'month'}>
          {translate(`Plus tard`)}
        </Option>
      </Selector>
    </Wrapper>
  )
}
