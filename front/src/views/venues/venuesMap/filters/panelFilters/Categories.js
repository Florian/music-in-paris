import React, { useContext } from 'react'
import styled from 'styled-components'

import I18nContext from 'utils/i18nContext'
import SearchContext from 'utils/searchContext'
import Title from 'components/misc/Title'
import Category from './categories/Category'

export const Wrapper = styled.div`
  margin-bottom: 1em;
`
const List = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  margin: 0 -0.3rem;
`
export default function Categories() {
  const { translate } = useContext(I18nContext)

  const search = useContext(SearchContext)

  return (
    <Wrapper>
      <Title>
        <span>{translate('Type de lieux')}</span>
      </Title>
      <List>
        <Category
          slug={'lives'}
          currentCategory={search.category}
          setCategory={category => {
            search.setCategory(category)
            search.setMoods([])
            search.setSpaces([])
            search.setGenres([])
            search.setActivities([])
          }}
        />
        <Category
          slug={'clubs'}
          currentCategory={search.category}
          setCategory={category => {
            search.setCategory(category)
            search.setMoods([])
            search.setSpaces([])
            search.setGenres([])
            search.setActivities([])
          }}
        />
        <Category
          slug={'records'}
          currentCategory={search.category}
          setCategory={category => {
            search.setCategory(category)
            search.setMoods([])
            search.setSpaces([])
            search.setGenres([])
            search.setActivities([])
          }}
        />
        <Category
          slug={'studios'}
          currentCategory={search.category}
          setCategory={category => {
            search.setCategory(category)
            search.setMoods([])
            search.setSpaces([])
            search.setGenres([])
            search.setActivities([])
          }}
        />
      </List>
    </Wrapper>
  )
}
