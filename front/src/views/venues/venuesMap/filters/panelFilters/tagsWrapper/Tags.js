import React, { useState, useEffect, useContext } from 'react'
import styled from 'styled-components'

import I18nContext from 'utils/i18nContext'
import { Wrapper } from '../Categories'
import Title from 'components/misc/Title'

import Tag from './tags/Tag'

const List = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 -0.3rem;
`
export default function Tags(props) {
  const { translate } = useContext(I18nContext)

  const [display, setDisplay] = useState(false)
  const [tags, setTags] = useState([])

  useEffect(() => {
    if (props.taxonomy && props.category) {
      switch (props.taxonomy.slug) {
        case 'mood':
          setDisplay(['lives', 'clubs'].includes(props.category))
          setTags(props.tags)
          return
        case 'space':
          setDisplay(
            ['lives', 'clubs', 'studios', 'records'].includes(props.category)
          )
          const newTags = props.tags
            .filter(tag => ['big', 'medium', 'small'].includes(tag.slug))
            .concat(
              props.tags.filter(
                tag => !['big', 'medium', 'small'].includes(tag.slug)
              )
            )
          if (props.category === 'lives') {
            setTags(
              newTags.filter(
                tag =>
                  !['piano', 'cabine-voix', 'lounge', 'analogique'].includes(
                    tag.slug
                  )
              )
            )
          }

          if (props.category === 'clubs') {
            setTags(
              newTags.filter(
                tag =>
                  ![
                    'dancefloor',
                    'piano',
                    'cabine-voix',
                    'lounge',
                    'analogique'
                  ].includes(tag.slug)
              )
            )
          }
          if (props.category === 'studios') {
            setTags(
              newTags.filter(
                tag =>
                  ![
                    'terrasse',
                    'assis',
                    'atypique',
                    'resto',
                    'dancefloor',
                    'plein-air'
                  ].includes(tag.slug)
              )
            )
          }
          if (props.category === 'records') {
            setTags(
              newTags.filter(
                tag =>
                  !['piano', 'cabine-voix', 'lounge', 'analogique'].includes(
                    tag.slug
                  )
              )
            )
          }
          return
        case 'genre':
          setDisplay(['lives', 'clubs', 'records'].includes(props.category))
          setTags(props.tags)

          if (props.category === 'clubs') {
            setTags(
              props.tags.filter(
                tag => !['chanson-francaise-et-varietes	'].includes(tag.slug)
              )
            )
          }
          return
        case 'activity':
          setDisplay(['studios'].includes(props.category))
          setTags(props.tags)
          return
        default:
          setDisplay(false)
      }
    } else {
      setDisplay(false)
    }
  }, [props.taxonomy, props.tags, props.category])

  return (
    display && (
      <Wrapper>
        <Title>
          <span>{translate(props.taxonomy && props.taxonomy.name)}</span>
        </Title>
        <List>
          {tags.map(tag => (
            <Tag
              key={tag.id}
              tag={tag}
              active={props.search.includes(tag.id)}
              toggleTag={tagId => {
                if (props.search.includes(tag.id)) {
                  props.setSearch(props.search.filter(tag => tagId !== tag))
                } else {
                  props.setSearch([...props.search, tagId])
                }
              }}
            />
          ))}
        </List>
      </Wrapper>
    )
  )
}
