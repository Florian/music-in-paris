import React from 'react'
import styled from 'styled-components'

import { colors } from 'utils/styles'

const Wrapper = styled.div`
  margin: 0.3rem;
  padding: 0.4rem 0.8rem;
  font-size: 14px;
  text-align: center;
  text-transform: uppercase;
  color: ${props => (props.active ? colors.background : colors.white)};
  background-color: ${props => (props.active ? colors.main : 'transparent')};
  border: 1px solid ${colors.main};
  cursor: pointer;
  transition: all 300ms ease-out;
`
const Icon = styled.span`
  display: inline-block;
  width: 0.6em;
  font-size: 16px;
  font-weight: bold;
  color: ${props => (props.active ? colors.background : colors.main)};
`
export default function Tag(props) {
  return (
    <Wrapper
      active={props.active}
      onClick={() => props.toggleTag(props.tag.id)}
    >
      <Icon active={props.active}>{props.active ? '-' : '+'}</Icon>{' '}
      <span>{props.tag.name}</span>
    </Wrapper>
  )
}
