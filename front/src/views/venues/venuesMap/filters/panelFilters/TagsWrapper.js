import React, { useContext } from 'react'

import TermsContext from 'utils/termsContext'
import SearchContext from 'utils/searchContext'
import Tags from './tagsWrapper/Tags'

export default function TagsWrapper() {
  const { moods, spaces, genres, activities, taxonomies } = useContext(
    TermsContext
  )
  const search = useContext(SearchContext)
  return (
    <>
      <Tags
        tags={moods}
        setSearch={search.setMoods}
        search={search.moods}
        taxonomy={taxonomies.mood}
        category={search.category}
      />
      <Tags
        tags={spaces}
        setSearch={search.setSpaces}
        search={search.spaces}
        taxonomy={taxonomies.space}
        category={search.category}
      />
      <Tags
        tags={genres}
        setSearch={search.setGenres}
        search={search.genres}
        taxonomy={taxonomies.genre}
        category={search.category}
      />
      <Tags
        tags={activities}
        setSearch={search.setActivities}
        search={search.activities}
        taxonomy={taxonomies.activity}
        category={search.category}
      />
    </>
  )
}
