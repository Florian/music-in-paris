import React, { useState, useEffect, useContext } from 'react'
import styled from 'styled-components'

import { colors, mq } from 'utils/styles'
import I18nContext from 'utils/i18nContext'
import Lives from 'components/svg/Lives'
import Clubs from 'components/svg/Clubs'
import Records from 'components/svg/Records'
import Studios from 'components/svg/Studios'

const Wrapper = styled.div`
  cursor: pointer;
  margin-bottom: 1em;
`
const Icon = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 20vw;
  height: 20vw;
  margin-bottom: 0.7em;
  border: ${props => (props.active ? `2px solid ${colors.main}` : 'none')};
  border-radius: 50%;
  transition: border 300ms ease-out;
  ${mq.large} {
    width: 7vw;
    height: 7vw;
  }
`
const Subtitle = styled.div`
  font-size: 14px;
  text-align: center;
  text-transform: uppercase;
`
export default function Category(props) {
  const { translate } = useContext(I18nContext)

  const [active, setActive] = useState(false)

  useEffect(() => {
    setActive(props.currentCategory === props.slug)
  }, [props.currentCategory, props.slug])
  return (
    <Wrapper onClick={() => props.setCategory(props.slug)}>
      <Icon active={active}>
        {props.slug === 'lives' && <Lives width='65%' height='65%' />}
        {props.slug === 'clubs' && <Clubs width='65%' height='65%' />}
        {props.slug === 'records' && <Records width='65%' height='65%' />}
        {props.slug === 'studios' && <Studios width='65%' height='65%' />}
      </Icon>
      <Subtitle>{translate(props.slug)}</Subtitle>
    </Wrapper>
  )
}
