import React, { useState, useEffect, useContext } from 'react'
import styled from 'styled-components'

import { colors, mq } from 'utils/styles'
import I18nContext from 'utils/i18nContext'
import Lives from 'components/svg/Lives'
import Clubs from 'components/svg/Clubs'
import Records from 'components/svg/Records'
import Studios from 'components/svg/Studios'

const Wrapper = styled.div`
  cursor: pointer;
  margin-bottom: 5vw;

  ${mq.large} {
    margin: 1em;
  }
`
const Icon = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 30vw;
  height: 30vw;
  margin-bottom: 0.7em;
  border: 2px solid ${colors.main};
  border-radius: 50%;

  ${mq.medium} {
    width: 200px;
    height: 200px;
  }
`
const Subtitle = styled.div`
  text-align: center;
  text-transform: uppercase;
`
export default function Categories(props) {
  const { translate } = useContext(I18nContext)

  const [active, setActive] = useState(false)

  useEffect(() => {
    setActive(props.currentCategory === props.slug)
  }, [props.currentCategory, props.slug])

  return (
    <Wrapper
      active={active}
      onClick={() => props.setCategory(active ? null : props.slug)}
    >
      <Icon>
        {props.slug === 'lives' && <Lives width='65%' height='65%' />}
        {props.slug === 'clubs' && <Clubs width='65%' height='65%' />}
        {props.slug === 'records' && <Records width='65%' height='65%' />}
        {props.slug === 'studios' && <Studios width='65%' height='65%' />}
      </Icon>
      <Subtitle>{translate(props.slug)}</Subtitle>
    </Wrapper>
  )
}
