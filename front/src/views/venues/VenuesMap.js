import React from 'react'
import styled from 'styled-components'

import { mq } from 'utils/styles'
import useVenuesFilters from 'hooks/useVenuesFilters'

import Map from 'components/Map'
import VenueMarkers from './venuesMap/VenueMarkers'
import Filters from './venuesMap/Filters'

const Wrapper = styled.div`
  ${mq.large} {
    display: flex;
  }
`
const MapWrapper = styled.div`
  flex: 1;
`
export default function VenuesMap(props) {
  const venues = useVenuesFilters({
    lives: props.lives,
    clubs: props.clubs,
    studios: props.studios,
    records: props.records,
    events: props.events,
    curators: props.curators,
    currentVenue: props.currentVenue
  })

  return (
    <Wrapper>
      <MapWrapper>
        <Map currentCenter={props.currentVenue}>
          <VenueMarkers
            venues={venues}
            currentVenue={props.currentVenue}
            events={props.events}
          />
        </Map>
      </MapWrapper>
      <Filters />
    </Wrapper>
  )
}
