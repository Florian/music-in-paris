import React, { useState } from 'react'
import styled from 'styled-components'

import { colors } from 'utils/styles'
import { getSrcset } from 'utils/img'
import Events from 'components/Events'
import Social from 'components/venue/Social'

const Wrapper = styled.div`
  margin-bottom: 2em;
`
const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
`
const Title = styled.h2``
const Logo = styled.img`
  height: 1.3em;
  width: auto;
`
const Description = styled.div``
const SelectionButton = styled.div`
  position: relative;
  cursor: pointer;

  &:before {
    content: '';
    position: absolute;
    top: 50%;
    left: 0;
    transform: translateY(-50%);
    width: 100%;
    height: 1px;
    background-color: ${colors.main};
  }
`
const Text = styled.span`
  position: relative;
  display: block;
  width: 14em;
  margin: 0 3em 1em 0;
  padding: 0.3em 1em 0.3em;
  color: ${props => (props.open ? colors.background : colors.main)};
  text-transform: uppercase;
  background-color: ${props => (props.open ? colors.main : colors.background)};
  border: 1px solid ${colors.main};
  border-radius: 2em;
  transition: all 300ms ease-out;
`
const Icon = styled.span`
  display: inline-block;
  width: 0.6em;
  font-size: 16px;
  font-weight: bold;
  color: ${props => (props.open ? colors.background : colors.main)};
  transition: all 300ms ease-out;
`
export default function Curator(props) {
  const [open, setOpen] = useState(false)
  return (
    props.curator && (
      <Wrapper>
        <Header>
          {props.curator.acf.logo ? (
            <Logo
              key={props.curator.acf.logo.id}
              src={props.curator.acf.logo.url}
              alt={props.curator.acf.logo.alt}
              srcSet={getSrcset(props.curator.acf.logo.sizes)}
              size={'50px'}
            />
          ) : (
            <Title
              dangerouslySetInnerHTML={{
                __html: props.curator.title.rendered
              }}
            />
          )}
          <Social
            links={props.curator.acf.reseaux_sociaux[0]}
            website={props.curator.acf.site_web}
          />
        </Header>
        <Description
          dangerouslySetInnerHTML={{
            __html: props.curator.acf.description
          }}
        />
        <SelectionButton onClick={() => setOpen(!open)}>
          <Text open={open}>
            <Icon open={open}>{open ? '-' : '+'}</Icon>{' '}
            {open ? 'Cacher ' : 'Voir '} la selection
          </Text>
        </SelectionButton>
        {open && (
          <Events events={props.events} curator={props.curator} noTitle />
        )}
      </Wrapper>
    )
  )
}
