import React, { useContext } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import useWindowHeight from 'hooks/useWindowHeight'
import { colors, mq } from 'utils/styles'
import ApiContext from 'utils/apiContext'

import Curator from './curators/Curator'
import BackButton from 'components/misc/BackButton'

const Wrapper = styled.div`
  position: relative;
  z-index: 120;
  min-height: ${props => props.windowHeight}px;
  background-color: ${colors.background};
`
const Header = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 50px;
`
const Title = styled.h1`
  margin: 0;
`
const Content = styled.div`
  padding: 1em 1em;

  ${mq.large} {
    width: 800px;
    margin: 0 auto;
  }
`
export default function Curators() {
  const windowHeight = useWindowHeight()

  const { curators, events } = useContext(ApiContext)

  return (
    <Wrapper windowHeight={windowHeight}>
      <Header>
        <Link to='/'>
          <BackButton />
        </Link>
        <Title>Liste des curateurs</Title>
      </Header>
      <Content>
        {curators.map(curator => (
          <Curator key={curator.id} curator={curator} events={events} />
        ))}
      </Content>
    </Wrapper>
  )
}
